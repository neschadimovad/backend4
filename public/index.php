<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
	if (!empty($_COOKIE['save'])) {
      setcookie('save', '', 100000);
    	$messages[] = 'Данные сохранены';
  	}
    $errors = array();
    $errors['field-name'] = !empty($_COOKIE['field-name_error']);
    $errors['field-email'] = !empty($_COOKIE['field-email_error']);
    $errors['field-date'] = !empty($_COOKIE['field-date_error']);
    $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
    $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
    $errors['field-name-4'] = !empty($_COOKIE['field-name-4_error']);
    $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
    $errors['check-1'] = !empty($_COOKIE['check-1_error']);
    $errors['1'] = !empty($_COOKIE['1_error']);
    $errors['2'] = !empty($_COOKIE['2_error']);
    $errors['3'] = !empty($_COOKIE['3_error']);

    if ($errors['field-name']) {
      setcookie('field-name_error', '', 100000);
      $messages[] = '<div class="error">Ввод имени.</div>';
    }
    if ($errors['1']){
      setcookie('1_error', '', 100000);
      $messages[] = '<div class="error">Введите имя с использованием латинских букв</div>';
    } 
    if ($errors['field-email']) {
      setcookie('field-email_error', '', 100000);
      $messages[] = '<div class="error">Ввод e-mail.</div>';
    }
    if ($errors['2']){
      setcookie('2_error', '', 100000);
      $messages[] = '<div class="error">Введите название почты,например:ivanov@mail.ru </div>';
    } 
    if ($errors['field-date']) {
      setcookie('field-date_error', '', 100000);
      $messages[] = '<div class="error">Ввод даты.</div>';
    }
    if ($errors['3']){
      setcookie('3_error', '', 100000);
      $messages[] = '<div class="error">Введите дату, например: 01.01.2001</div>';
    } 
    if ($errors['radio-group-1']) {
      setcookie('radio-group-1_error', '', 100000);
      $messages[] = '<div class="error">Выбор пола.</div>';
    }
    if ($errors['radio-group-2']) {
      setcookie('radio-group-2_error', '', 100000);
      $messages[] = '<div class="error">Выбор кол-ва конечностей.</div>';
    }
    if ($errors['field-name-4']) {
      setcookie('field-name-4_error', '', 100000);
      $messages[] = '<div class="error">Выбор суперспособности.</div>';
    }
    if ($errors['field-name-2']) {
      setcookie('field-name-2_error', '', 100000);
      $messages[] = '<div class="error">Напишите биографию.</div>';
    }
    if ($errors['check-1']) {
      setcookie('check-1_error', '', 100000);
      $messages[] = '<div class="error">Примите условия.</div>';
    }
  
    $values = array();

    $values['field-name'] = empty($_COOKIE['field-name_value']) ? '' : $_COOKIE['field-name_value'];
    $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
    $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
    $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? '' : $_COOKIE['radio-group-1_value'];
    $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '' : $_COOKIE['radio-group-2_value'];
    $values['field-name-4'] = empty($_COOKIE['field-name-4_value']) ? '' : $_COOKIE['field-name-4_value'];
    $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
    $values['check-1'] = empty($_COOKIE['check-1_value']) ? '' : $_COOKIE['check-1_value'];
  

  	include('form.php');
}
else{

  $errors = FALSE;

  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['field-name'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['field-email'])){
    setcookie('2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/",$_POST['field-date'])){
    setcookie('3_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['field-name'])) {
    setcookie('field-name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name_value', $_POST['field-name'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-email'])) {
    setcookie('field-email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-email_value', $_POST['field-email'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-date'])) {
    setcookie('field-date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-date_value', $_POST['field-date'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-group-1'])) {
    setcookie('radio-group-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-group-1_value', $_POST['radio-group-1'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-group-2'])) {
    setcookie('radio-group-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-group-2_value', $_POST['radio-group-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-4'])) {
    setcookie('field-name-4_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name-4_value', $_POST['field-name-4'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-2'])) {
    setcookie('field-name-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name-2_value', $_POST['field-name-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['check-1'])) {
    setcookie('check-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check-1_value', $_POST['check-1'], time() + 365 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('field-name_error', '', 100000);
    setcookie('1_error', '', 100000);
    setcookie('field-email_error', '', 100000);
    setcookie('2_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('3_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('check-1_error', '', 100000);
  }
  $imya = $_POST['field-name'];
  $mail = $_POST['field-email'];
  $data = $_POST['field-date'];
  $radknop1= $_POST['radio-group-1'];
  $radknop2 = $_POST['radio-group-2'];
  $spisok = $_POST['field-name-4'];
  $imya2 = $_POST['field-name-2'];
  $soglas = $_POST['check-1'];

    $user = 'u23965';
    $pass = '3288893';
    $db = new PDO('mysql:host=localhost;dbname=u23965', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
      $stmt = $db->prepare("INSERT INTO form (imya,mail,data,radknop1,radknop2,spisok,imya2,soglas) VALUE(:imya,:mail,:data,:radknop1,:radknop2,:spisok,:imya2,:soglas)");
      $stmt -> execute(['imya'=>$imya,'mail'=>$mail,'data'=>$data,'radknop1'=>$radknop1,'radknop2'=>$radknop2,'spisok'=>$spisok,'imya2'=>$imya2,'soglas'=>$soglas]);
        echo "<script type='text/javascript'>alert('Спасибо, данные отправлены:)');</script>";
    }catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    header('Location: index.php');
}
