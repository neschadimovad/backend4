<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Задание 4</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>

<body class="text-dark">
    <div class="container-fluid">
        <header class="row d-flex flex-row justify-content-center">
            <div
                class="d-flex flex-row align-items-center justify-content-around justify-content-sm-start col-sm-9 px-sm-4 h-100">
                <img src="https://www.flaticon.com/svg/vstatic/svg/556/556772.svg?token=exp=1619691565~hmac=c7f46734b8b443e7c5350469bd2ba593"
                    class="photo" alt="Ошибка">
                <h1 class="text-body">Задание 4</h1>
            </div>
        </header>
        <div class="row d-flex flex-row justify-content-center mt-3 order-sm-3">
            <div class="col-sm-9 " style="background-color:#edf0f3af;">
                <div class="items d-flex flex-column ">
                    <div id="form" class="order-sm-3">
                        <?php

                        if (!empty($messages)) {
                            print('<div id="messages">');
                            foreach ($messages as $message) {
                                print($message);
                            }
                            print('</div>');
                        }
                    ?>
                     <h2 class="text-center">Форма</h2>
                      <form action="" method="POST">
                            <label>
                                Имя:<br />
                                <input name="field-name" <?php if ($errors['field-name'] || $errors['1']) {print "class='error'" ;} ?> value="<?php print $values['field-name']; ?>" />
                            </label><br />

                            <label>
                                E-mail:<br />
                                <input name="field-email" <?php if ($errors['field-email'] || $errors['2']) {print 'class="error"' ;} ?> value="<?php print $values['field-email']; ?>"  />
                            </label><br />

                            <label>
                                День рождения:<br />
                                <input name="field-date" <?php if ($errors['field-date'] || $errors['3']) {print 'class="error"' ;} ?> value="<?php print $values['field-date']; ?>" />
                            </label><br />
                            Пол:<br />
                            <label><input type="radio" name="radio-group-1" <?php if ($errors['radio-group-1']) {print 'class="error"' ;} if($values['radio-group-1']=="М"){print "checked='checked'";}?> value="М" />
                                Муж.</label>
                            <label><input type="radio" name="radio-group-1" <?php if ($errors['radio-group-1']) {print 'class="error"' ;} if($values['radio-group-1']=="Ж"){print "checked='checked'";}?> value="Ж" />
                                Жен</label><br />
                            Кол-во конечностей:<br />
                            <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="4"){print "checked='checked'";}?> value="4" />
                                4</label>
                            <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="8"){print "checked='checked'";}?> value="8" />
                                8</label>
                            <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="22"){print "checked='checked'";}?> value="22" />
                                22</label><br />
                            <label>
                                Сверхспособности:
                                <br />
                                <select name="field-name-4" multiple="multiple">
                                <option value="Бессмертие" <?php if($values['field-name-4']=="Бессмертие"){print "selected='selected'";}?>>Бессмертие</option>
                                <option value="Левитация" <?php if($values['field-name-4']=="Левитация"){print "selected='selected'";}?>>Левитация</option>
                                <option value="Прохождение сквозь стены" <?php if($values['field-name-4']=="Прохождение сквозь стены"){print "selected='selected'";}?>>Прохождение сквозь стены</option>
                                </select>
                            </label><br />
                            <label>
                                Биография:<br />
                                <textarea name="field-name-2" <?php if ($errors['field-name-2']) {print 'class="error"' ;} ?>><?php print $values['field-name-2']; ?></textarea> 
                            </label><br />
                            Ознакомлен с условиями:<br />
                            <label><input type="checkbox" name="check-1" <?php  if($values['check-1']=="1"){print "checked='checked'";}?> value="1" />
                                Согласен с условиями</label><br /> 
                            <input type="submit" value="Отправить" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer class="row d-flex flex-row justify-content-start mt-3 h-sm-75">
            <div class="d-flex flex-row align-items-center col-sm-9">
                <div class="text-body">
                    <p class="text-light h6">(c) Нещадимова Дарья 27/2 группа</p>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
